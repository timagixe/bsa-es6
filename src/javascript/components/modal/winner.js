import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
    showModal({
        title: `${fighter.name.toUpperCase()} WON THE FIGHT`,
        bodyElement: createImage(fighter),
    });
}

function createImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name,
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter___fighter-image',
        attributes,
    });

    return imgElement;
}
