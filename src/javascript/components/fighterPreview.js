import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    fighterElement.appendChild(createFighterImage(fighter));
    fighterElement.appendChild(createInfoBox(fighter));
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createInfoBox(fighter) {
  const { name, health, attack, defense } = fighter;
  const infoBox = createElement({
    tagName: 'span',
    className: 'fighter-preview__infobox',
  });

  infoBox.innerText = `Name: ${fighter.name}
  Health: ${health}
  Attack: ${attack}
  Armor: ${defense}`;

  return infoBox;
}
