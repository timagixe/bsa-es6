import { controls } from '../../constants/controls';

const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination,
} = controls;

export async function fight(firstFighter, secondFighter) {
    const firstFighterCopy = addProps(firstFighter);
    const secondFighterCopy = addProps(secondFighter);

    activateComboHitListener(firstFighterCopy, secondFighterCopy);

    disableComboHitListener(firstFighterCopy, secondFighterCopy);

    activateAttackBlockDownListener(firstFighterCopy, secondFighterCopy);

    activateAttackBlockUpListener(firstFighterCopy, secondFighterCopy);

    return new Promise((resolve) => {
        // listener checks fighters health in order to resolve or not the promise
        document.addEventListener('keydown', () => {
            const oneOfFightersIsDead =
                firstFighterCopy.health <= 0 || secondFighterCopy.health <= 0;
            if (oneOfFightersIsDead) {
                resolve(
                    firstFighterCopy.health < secondFighterCopy.health
                        ? secondFighter
                        : firstFighter
                );
            }
        });
    })
        .then((res) => res)
        .catch((err) => console.log(err));
}

export function getDamage(attacker, defender) {
    // return damage
    const [hitPower, blockPower, isAttackerInBlock, isDefenderInBlock] = [
        getHitPower(attacker),
        getBlockPower(defender),
        attacker.blockEnabled,
        defender.blockEnabled,
    ];

    let damage = hitPower;

    // checks whether the defender is using block
    if (isDefenderInBlock) {
        damage = hitPower - blockPower;
    }

    // checks whether the attacker is using block
    if (isAttackerInBlock) {
        damage = 0;
    }

    // damage is 0 when blockPower > hitPower
    damage = damage > 0 ? damage : 0;

    return damage;
}

function getCriticalDamage(attacker) {
    // return critical damage
    let { lastCriticalHitTime, attack } = attacker;
    const currentTime = new Date().getTime(); // returns new Date() in milliseconds
    const tenSecondsInMilliseconds = 10000;
    let damage = 0;

    // checks if 10seconds passed
    if (currentTime - +lastCriticalHitTime > tenSecondsInMilliseconds) {
        damage = 2 * attack;
        attacker.lastCriticalHitTime = currentTime;
    }

    return damage;
}

export function getHitPower(fighter) {
    // return hit power
    const criticalHitChance = Math.random() + 1;
    const { attack } = fighter;
    const power = attack * criticalHitChance;

    return power;
}

export function getBlockPower(fighter) {
    // return block power
    const dodgeChance = Math.random() + 1;
    const { defense } = fighter;
    const power = defense * dodgeChance;

    return power;
}

function removeHealthFromDefender(defender, damage) {
    // return fighter object with updated health

    healthBarHandler(defender, damage); // updates health-bar

    defender.health = defender.health - damage;
}

function healthBarHandler(fighter, damage) {
    // update health-bar
    let indicatorsNodeList = document.querySelectorAll(
        '.arena___fighter-indicator'
    );
    let indicatorsArray = Array.prototype.slice.call(indicatorsNodeList);
    let [defenderIndicator] = indicatorsArray.filter(
        (val) => val.innerText === fighter.name
    );

    let calculatedHealthBar;
    calculatedHealthBar = ((fighter.health - damage) * 100) / fighter.maxHealth;

    if (fighter.health - damage <= 0) calculatedHealthBar = 0;
    defenderIndicator.querySelector(
        '.arena___health-bar'
    ).style.width = `${calculatedHealthBar}%`;
}

function activateComboHitListener(fighterOne, fighterTwo) {
    // listener checks for combo pushes (key is down)
    document.addEventListener('keydown', ({ code }) => {
        if (
            PlayerOneCriticalHitCombination.includes(code) &&
            !fighterOne.combo.includes(code)
        ) {
            const combo = fighterOne.combo.concat([code]);

            fighterOne.combo = combo;
            if (combo.length === PlayerOneCriticalHitCombination.length) {
                removeHealthFromDefender(
                    fighterTwo,
                    getCriticalDamage(fighterOne, fighterTwo)
                );
            }
        }
        if (
            PlayerTwoCriticalHitCombination.includes(code) &&
            !fighterTwo.combo.includes(code)
        ) {
            const combo = fighterTwo.combo.concat([code]);

            fighterTwo.combo = combo;
            if (combo.length === PlayerTwoCriticalHitCombination.length) {
                removeHealthFromDefender(
                    fighterOne,
                    getCriticalDamage(fighterTwo, fighterOne)
                );
            }
        }
    });
}

function disableComboHitListener(fighterOne, fighterTwo) {
    // listener checks for combo releases (key is up)
    document.addEventListener('keyup', ({ code }) => {
        if (fighterOne.combo.includes(code)) {
            fighterOne.combo = fighterOne.combo.filter(
                (value) => value != code
            );
        }
        if (fighterTwo.combo.includes(code)) {
            fighterTwo.combo = fighterTwo.combo.filter(
                (value) => value != code
            );
        }
    });
}

function activateAttackBlockDownListener(fighterOne, fighterTwo) {
    // listener checks whether attack / block keys are down
    document.addEventListener('keydown', (event) => {
        switch (event.code) {
            case PlayerOneAttack:
                removeHealthFromDefender(
                    fighterTwo,
                    getDamage(fighterOne, fighterTwo)
                );
                break;
            case PlayerOneBlock:
                fighterOne.blockEnabled = true;
                break;
            case PlayerTwoAttack:
                removeHealthFromDefender(
                    fighterOne,
                    getDamage(fighterTwo, fighterOne)
                );
                break;
            case PlayerTwoBlock:
                fighterTwo.blockEnabled = true;
                break;
        }
    });
}

function activateAttackBlockUpListener(fighterOne, fighterTwo) {
    // listener checks whether attack / block keys are up
    document.addEventListener('keyup', (event) => {
        switch (event.code) {
            case PlayerOneBlock:
                fighterOne.blockEnabled = false;
                break;
            case PlayerTwoBlock:
                fighterTwo.blockEnabled = false;
                break;
        }
    });
}

// adds additional props to fighter object
function addProps(fighter) {
    return {
        ...fighter,
        blockEnabled: false,
        combo: [],
        lastCriticalHitTime: null,
        healthBar: 100,
        maxHealth: fighter.health,
    };
}
